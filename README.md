# Prepare to install

Install packages:
```sh
$ pip install Django
$ pip install --allow-external mysql-connector-python mysql-connector-python
$ apt-get install libmysqlclient-dev python-mysqldb
```
Chek django version
```sh
$ django-admin --version
1.8.4
```
# Install APP
```sh
$ git clone https://bitbucket.org/ikleeen/django_app
```
Create database and user:
```sh
$ cat django_app/cpv_admin/cpv_admin/settings.py
...
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'cpv_django_base',
        'USER': 'cpv_django_user',
	'PASSWORD': 'cpv_django_password',
        'HOST': 'localhost',   # Or an IP Address that your DB is hosted on
        'PORT': '3306',
    }
....
```
Create tables
```sh
$ cd django_app/cpv_admin/
$ python manage.py syncdb
```
Run server 
```sh
$ python manage.py runserver 0.0.0.0:8001
```
Try it: 
http://127.0.0.1:8001/interface/list