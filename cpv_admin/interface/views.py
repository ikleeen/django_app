# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.conf import settings
import mysql.connector
from datetime import datetime
from interface.models import Document
from interface.models import Source_Forms

from interface.models import Campaigns
from interface.models import Destinations
from interface.forms import DocumentForm
from interface.forms import SourceForms


# Form - input form, Text - response data for user, Request - incoming request
def response(Form, Text, Request):
    return render_to_response(
        'interface/list.html',
        {'form': Form, 'ret': Text},
        context_instance=RequestContext(Request)
        )

def add_campaigns(db, campaign_names):
    return_ids = []
    cursor = db.cursor()
    query = ("SELECT * FROM campaigns ORDER BY CampaignID DESC LIMIT 1")
    cursor.execute(query)
    rows = cursor.fetchall()
    for row in rows:
        CampaignTypeID = 8   #Statical - 8 = Direct Link & Landing Page Campaign
        SourceID  = row[3] # ??
        Source = row[4]    # ??
        KeywordToken = row[5] # ??
        CreateUserID = row[11] # Get last user
        RealTimeCPV = row[22] # ??
        CostTypeID = row[23] #?? 1-CPV, 2- CPC
        GroupID = row[41]
        CreateDate = datetime.now().date()

        for new_camp in campaign_names:
            query = ("INSERT INTO campaigns "
                     "(CampaignName,CampaignTypeID,SourceID,Source,KeywordToken,CreateUserID,RealTimeCPV,CostTypeID,GroupID,CreateDate) "
                     "VALUES "
                     "(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)")
            values = (new_camp, CampaignTypeID, SourceID, Source, KeywordToken, CreateUserID, RealTimeCPV, CostTypeID, GroupID, CreateDate)
            cursor.execute(query, values)
            id = cursor.lastrowid
            return_ids.append(id)
            db.commit()
            local_copy_camp = Campaigns(CampaignID = id,
                                        CampaignName = new_camp,
                                        CampaignTypeID = CampaignTypeID,
                                        SourceID = SourceID,
                                        Source = Source,
                                        KeywordToken = KeywordToken,
                                        CreateUserID = CreateUserID,
                                        RealTimeCPV = RealTimeCPV,
                                        CostTypeID = CostTypeID,
                                        CreateDate = CreateDate,
                                        GroupID = GroupID)
            local_copy_camp.save()
    return return_ids

def add_landings(db, landings, campaigns_ids):
    # Length of landings must be divisible by 2
    cursor = db.cursor()
    Level = 1
    query = ("SELECT AffiliateSourceID FROM destinations WHERE Level=1 ORDER BY DestinationID DESC LIMIT 1")
    cursor.execute(query)
    rows = cursor.fetchall()
    for row in rows:
        AffiliateSourceID = row[0]  #Get Last AffiliateSourceID
    # Calculate share values
    for id_camp in campaigns_ids:
        id = 0;
        for name,url in zip(landings[0::2], landings[1::2]):
            query = ("INSERT INTO destinations "
                     "(CampaignID,Offer,Url,Payout,Share,CurrentShare,LandingPageID,AffiliateSourceID,Level) "
                     "VALUES "
                     "(%s, %s, %s, %s, %s, %s, %s, %s, %s)")
            if id==0:
                share = 100 / (len(landings)/2) + 100 % (len(landings)/2)
            else:
                share = 100 / (len(landings)/2)
            values = (id_camp, name, url, 0, share, 1, id, AffiliateSourceID, Level)
            cursor.execute(query, values)
            local_copy_landings = Destinations(
                DestinationID = cursor.lastrowid,
                CampaignID = id_camp,
                Offer = name,
                Url = url,
                Payout = 0,
                Share = share,
                CurrentShare = 1,
                LandingPageID = id,
                AffiliateSourceID = AffiliateSourceID,
                Level = Level)
            local_copy_landings.save()
            id += 1
    db.commit()
    return 0


def add_offers(db, offers, campaigns_ids):
    # Length of landings must be divisible by 3
    Level = 2
    cursor = db.cursor()
    query = ("SELECT AffiliateSourceID FROM destinations WHERE Level=2 ORDER BY DestinationID DESC LIMIT 1")
    cursor.execute(query)
    rows = cursor.fetchall()
    for row in rows:
        AffiliateSourceID = row[0]  #Get Last AffiliateSourceID
    for id_camp in campaigns_ids:
        id = 0;
        for name,url,payout in zip(offers[0::3], offers[1::3], offers[2::3]):
            query = ("INSERT INTO destinations "
                     "(CampaignID,Offer,Url,Payout,Share,CurrentShare,LandingPageID,AffiliateSourceID,Level) "
                     "VALUES "
                     "(%s, %s, %s, %s, %s, %s, %s, %s, %s)")
            if id==0:
                share = 100 / (len(offers)/3) + 100 % (len(offers)/3)
            else:
                share = 100 / (len(offers)/3)
            values = (id_camp, name, url, payout, share, 1, id, AffiliateSourceID, Level)
            cursor.execute(query, values)
            local_copy_offers = Destinations(
                DestinationID = cursor.lastrowid,
                CampaignID = id_camp,
                Offer = name,
                Url = url,
                Payout = payout,
                Share = share,
                CurrentShare = 1,
                LandingPageID = id,
                AffiliateSourceID = AffiliateSourceID,
                Level = Level)
            local_copy_offers.save()
            id += 1
    db.commit()
    return 0

# Get last campaign row from CPV database
def insert_data(names,landings,offers):
    db = mysql.connector.connect(host=settings.HOST, user=settings.DB_USER, password=settings.DB_PASSWORD, database=settings.DB_NAME)
    campaigns_ids = add_campaigns(db, names)
    add_landings(db, landings, campaigns_ids)
    add_offers(db, offers, campaigns_ids)
    return campaigns_ids

def parce_data(raw_camp_names, raw_camp_landings, raw_camp_offers):
# So we have
# camp_names = 'dk denmark\ndk erovie juicy POP\n',
# camp_landings = '\ndk4mmtnn\nhttp://freerealdating.com/dk/dk4mmtnn\n\ndk5coonh\nhttp://freerealdating.com/dk/dk5coonh\n',
# camp_offers = '\nADSIMILIS 2154 Erovie 30+ [DK] $8.20 88766\nhttp://go.adsimilis.com/aff_c?offer_id=88766&aff_id=5978&aff_sub=\n8.20\n\nADSIMILIS 2154 Erovie 30+ [DK] $8.20 11110\nhttp://go.adsimilis.com/aff_c?offer_id=11110&aff_id=5978&aff_sub=\n8.20\n\nADSIMILIS 2154 Erovie 30+ [DK] $8.20 22993\nhttp://go.adsimilis.com/aff_c?offer_id= 22993&aff_id=5978&aff_sub=\n8.20\n'
# Then we must parce all raw data by delimiter '/n' and filter empty elements
# campaign_names = [name1, name2, name3 ...]
    campaign_names = filter(lambda x: len(x)>0, raw_camp_names.split('\n'))
# campaign_landings = [name1, url1, name2, url2, ...]
    campaign_landings = filter(lambda x: len(x)>0, raw_camp_landings.split('\n'))
# campaign_offers = [name1, url1, cost1, name2, url2, cost2, ...]
    campaign_offers = filter(lambda x: len(x)>0, raw_camp_offers.split('\n'))
    return [campaign_names, campaign_landings, campaign_offers]

#NEED ADD HOSTNAME
def cre_log(campaigns_ids):
    text = 'Succes create campaigns: <br />-------------<br />'
    for i in campaigns_ids:
        text = text + str(i) + '<br />'
    return text + '-------------<br />'

def upload(request):
    text = ''
    # Handle file upload
    if request.method == 'POST':
        datafile_form = DocumentForm(request.POST, request.FILES)
        if datafile_form.is_valid():
            newdoc = Document(docfile = request.FILES['docfile'])
            newdoc.save()
            # Redirect to the document list after POST
            return HttpResponseRedirect(reverse('interface.views.upload'))
    else:
        datafile_form = DocumentForm() # A empty, unbound form
    # Get last loaded doc name
    try:
        documents = Document.objects.latest('upload_date')
    except:
    # Empty doc table - nothing to do
        return response(datafile_form, 'Select valid CSV file', request)
    # Table not empty - check 'parced' attribute, if it false - try parce that doc
    if (documents.parced == False):
        raw_text = documents.docfile.read()
        # Set 'parced' attribute to True (if data in file invalid - we a really try parse that)
        documents.parced = True
        documents.save()
        # Split raw text and try parce data for sql table
        splitted_data = raw_text.split(settings.CSV_DELIMITER)
        # Function for form - three input form
        try:
            text = parce_data(splitted_data[0], splitted_data[1], splitted_data[2])
        except:
            return response(datafile_form, 'Invalid data in CSV file', request)
        if (len(text[0]) == 0) or (len(text[1])==0 or len(text[1])%2 != 0) or (len(text[2])==0 or len(text[2])%3 != 0):
            return response(datafile_form, 'Check file before inserting failed. Check the data. CSV data must be valid', request)
        campaigns_ids = insert_data(text[0],text[1], text[2])
        log = cre_log(campaigns_ids)
        return response(datafile_form, log, request)
    else:
        return response(datafile_form, 'Last file parced. Please select valid CSV file', request)

def form_upload(request):
    text = ''
    # Handle file upload
    if request.method == 'POST':
        source_form = SourceForms(request.POST)
        if (source_form.is_valid()):
            if (source_form.cleaned_data['campNames'] and source_form.cleaned_data['landings'] and source_form.cleaned_data['offers']):
                newform = Source_Forms(camp_names = request.POST['campNames'], landings = request.POST['landings'], offers = request.POST['offers'])
                newform.save()
                return HttpResponseRedirect(reverse('interface.views.form_upload'))
    else:
        source_form = SourceForms()
    try:
        datafields = Source_Forms.objects.latest('upload_date')
    except:
        return response(source_form, 'Can not read last loaded data from forms', request)
    if (datafields.parced == False):
        campaigns = datafields.camp_names
        landings = datafields.landings
        offers = datafields.offers
        datafields.parced = True
        datafields.save()
        try:
            text = parce_data(campaigns, landings, offers)
        except:
            return response(source_form, 'Can not parce last loaded data from forms', request)
        if (len(text[0]) == 0) or (len(text[1])==0 or len(text[1])%2 != 0) or (len(text[2])==0 or len(text[2])%3 != 0):
            return response(source_form, 'Check the data before inserting failed. Check the data', request)
        campaigns_ids = insert_data(text[0],text[1], text[2])
        log = cre_log(campaigns_ids)
        return response(source_form, log, request)
    else:
        return response(source_form, 'Last data parced. Please insert the valid data into the forms', request)

def main_page(request):
    source_form = SourceForms()
    datafile_form = DocumentForm()
    return render_to_response(
        'interface/list.html',
        {'source_form': source_form, 'file_form': datafile_form, 'ret': 'Please select source CSV file or past data into the forms'},
        context_instance=RequestContext(request)
        )




