# -*- coding: utf-8 -*-
from django.db import models

class Document(models.Model):
    docfile = models.FileField(upload_to='documents/%Y/%m/%d')
    upload_date = models.TimeField(auto_now=True, null=False)
    parced = models.BooleanField(null=False, default=False)

class Source_Forms(models.Model):
    camp_names = models.CharField(
        max_length=5000)
    landings = models.CharField(
        max_length=6000)
    offers = models.CharField(
        max_length=6000)
    upload_date = models.TimeField(auto_now=True, null=False)
    parced = models.BooleanField(null=False, default=False)


class Destinations(models.Model):
    DestinationID = models.IntegerField(primary_key = True, null=False)
    CampaignID = models.IntegerField(null=False)
    PathID = models.IntegerField(null=False, default=0)
    Offer = models.CharField(max_length=500, null=False)
    Url = models.CharField(max_length=500, null=False)
    Payout = models.FloatField(null=False, default=0)
    Share = models.IntegerField(null=False, default=0)
    CurrentShare = models.IntegerField(null=False, default=0)
    LandingPageID = models.IntegerField(null=False, default=0)
    AffiliateSourceID = models.PositiveSmallIntegerField(null=False, default=0)
    Level = models.IntegerField(null=False, default=1)
    Sent = models.IntegerField(null=False, default=1)
    SharePath = models.IntegerField(null=False, default=50)
    CurrentSharePath = models.IntegerField(null=False, default=0)
    Inactive = models.IntegerField(null=False, default=0)



class Campaigns(models.Model):
    CampaignID = models.IntegerField(primary_key = True, null=False)
    CampaignName = models.CharField(max_length=250, null=False, default='')
    CampaignTypeID = models.IntegerField(null=True, default=1)
    SourceID = models.CharField(max_length=100, null=False, default='')
    Source = models.CharField(max_length=250, null=False, default='')
    KeywordToken = models.CharField(max_length=100, null=False, default='')
    UniqueToken = models.CharField(max_length=100, null=False, default='')
    PassUnique = models.IntegerField(null=False, default=0)
    AppendToken = models.CharField(max_length=255, null=False, default='')
    EngageSeconds = models.PositiveSmallIntegerField(null=False, default=0)
    CreateDate = models.TimeField(auto_now=True, null=False)
    CreateUserID = models.IntegerField(null=False)
    ModifyDate = models.TimeField(auto_now=False, null=True)
    ModifyUserID = models.IntegerField(null=True)
    DeleteDate = models.TimeField(auto_now=False, null=True)
    DeleteUserID = models.IntegerField(null=True)
    LastViews = models.IntegerField(null=False, default=0)
    LastViewsNew = models.IntegerField(null=False, default=0)
# LastProfit
# LastProfitNew
# LastROI
# LastReportUpdate

    RealTimeCPV = models.FloatField(null=False, default=0)
    CostTypeID = models.IntegerField(null=False, default=1)
    DestinationType = models.IntegerField(null=False, default=1)
    TrackingType = models.IntegerField(null=False, default=2)
    AssignedTo = models.IntegerField(null=False, default=0)
    CaptureEmbed = models.IntegerField(null=False, default=0)
    CapturePopUp = models.IntegerField(null=False, default=0)
    CaptureExitPop = models.IntegerField(null=False, default=0)
    PassTarget = models.IntegerField(null=False, default=0)
    PassTargetParam = models.CharField(max_length=45,null=False, default='target')
    PassTargetOffer = models.IntegerField(null=False, default=0)
    PassTargetOfferParam = models.CharField(max_length=45, null=False, default='target')
    PassSubIdLP = models.IntegerField(null=False, default=0)
    PassSubId = models.IntegerField(null=False, default=1)
    PassCookie = models.IntegerField(null=False, default=0)
    PassCookieParam = models.CharField(max_length=45, null=False, default='cookie')
    RedirectType = models.IntegerField(null=False, default=2)
    KeyCode = models.CharField(max_length=45, null=False, default='')
    FailurePage = models.CharField(max_length=255, null=False, default='')
    GroupID = models.PositiveSmallIntegerField(null=False, default=0)
    Inactive = models.IntegerField(null=False, default=0)
    SplitShare = models.IntegerField(null=False, default=50)
    ShareLanding = models.IntegerField(null=False, default=0)
    ShareOffer = models.IntegerField(null=False, default=0)
    Priority = models.PositiveSmallIntegerField(null=False, default=1)
    OptimizationProfileID = models.PositiveSmallIntegerField(null=True)
    AlertProfileID = models.PositiveSmallIntegerField(null=False, default=1)
    CaptureReferrer = models.IntegerField(null=False, default=1)
    ExtraTokens = models.IntegerField(null=False, default=0)
    ExtraTokenName1 = models.CharField(max_length=250, null=False, default='')
    ExtraTokenUrl1 = models.CharField(max_length=250, null=False, default='')
    ExtraTokenParam1 = models.CharField(max_length=100, null=False, default='')
    ExtraTokenPass1 = models.IntegerField(null=False, default=2)
    ExtraTokenName2 = models.CharField(max_length=250, null=False, default='')
    ExtraTokenUrl2 = models.CharField(max_length=250, null=False, default='')
    ExtraTokenParam2 = models.CharField(max_length=100, null=False, default='')
    ExtraTokenPass2 = models.IntegerField(null=False, default=2)
    ExtraTokenName3 = models.CharField(max_length=250, null=False, default='')
    ExtraTokenUrl3 = models.CharField(max_length=250, null=False, default='')
    ExtraTokenParam3 = models.CharField(max_length=100, null=False, default='')
    ExtraTokenPass3 = models.IntegerField(null=False, default=2)
    ExtraTokenName4 = models.CharField(max_length=250, null=False, default='')
    ExtraTokenUrl4 = models.CharField(max_length=250, null=False, default='')
    ExtraTokenParam4 = models.CharField(max_length=100, null=False, default='')
    ExtraTokenPass4 = models.IntegerField(null=False, default=2)
    ExtraTokenName5 = models.CharField(max_length=250, null=False, default='')
    ExtraTokenUrl5 = models.CharField(max_length=250, null=False, default='')
    ExtraTokenParam5 = models.CharField(max_length=100, null=False, default='')
    ExtraTokenPass5 = models.IntegerField(null=False, default=2)
    ExtraTokenName6 = models.CharField(max_length=250, null=False, default='')
    ExtraTokenUrl6 = models.CharField(max_length=250, null=False, default='')
    ExtraTokenParam6 = models.CharField(max_length=100, null=False, default='')
    ExtraTokenPass6 = models.IntegerField(null=False, default=2)
    ExtraTokenName7 = models.CharField(max_length=250, null=False, default='')
    ExtraTokenUrl7 = models.CharField(max_length=250, null=False, default='')
    ExtraTokenParam7 = models.CharField(max_length=100, null=False, default='')
    ExtraTokenPass7 = models.IntegerField(null=False, default=2)
    ExtraTokenName8 = models.CharField(max_length=250, null=False, default='')
    ExtraTokenUrl8 = models.CharField(max_length=250, null=False, default='')
    ExtraTokenParam8 = models.CharField(max_length=100, null=False, default='')
    ExtraTokenPass8 = models.IntegerField(null=False, default=2)
    ExtraTokenName9 = models.CharField(max_length=250, null=False, default='')
    ExtraTokenUrl9 = models.CharField(max_length=250, null=False, default='')
    ExtraTokenParam9 = models.CharField(max_length=100, null=False, default='')
    ExtraTokenPass9 = models.IntegerField(null=False, default=2)
    ExtraTokenName10 = models.CharField(max_length=250, null=False, default='')
    ExtraTokenUrl10 = models.CharField(max_length=250, null=False, default='')
    ExtraTokenParam10 = models.CharField(max_length=100, null=False, default='')
    ExtraTokenPass10 = models.IntegerField(null=False, default=2)
    AdTokenName = models.CharField(max_length=250, null=False, default='')
    AdTokenUrl = models.CharField(max_length=250, null=False, default='')
    AdTokenParam = models.CharField(max_length=100, null=False, default='')
    AdTokenPass = models.IntegerField(null=False, default=0)









