# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url

urlpatterns = patterns('interface.views',
    url(r'^form_upload/$', 'form_upload', name='form_upload'),
    url(r'^upload/$', 'upload', name='upload'),
    url(r'^$', 'main_page', name='main_page'),
)
